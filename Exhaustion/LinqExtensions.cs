﻿using System.Collections.Generic;
using System.Linq;

namespace Exhaustion
{
    public static class LinqExtensions
    {
        public static IEnumerable<T> SkipLast<T>(this T[] items, int n)
        {
            return items.Take(items.Length - n);
        }

        public static IEnumerable<T> TakeEveryOther<T>(this IEnumerable<T> items)
        {
            var take = true;
            foreach (var item in items)
            {
                if (take)
                {
                    yield return item;
                }
                take = !take;
            }
        }

        public static void CreateOrAdd<T>(this IDictionary<T, int> dict, T key, int value)
        {
            if (!dict.ContainsKey(key))
            {
                dict.Add(key, value);
            }
            else
            {
                dict[key] += value;
            }
        }
    }
}
