﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace Exhaustion
{
    public class Booth
    {
        public static string[] Liblabs = { "ALP", "LIB", "LNP", "NPA" };
        public const bool Verbose = false;
        private readonly WebClient client = new WebClient();
        private readonly int index, year;
        private readonly string location;
        private readonly Candidate[] candidates;
        private readonly Candidate[] tpp;

        public Booth(int year, int index)
        {
            this.year = year;
            this.index = index;
            var dir = year.ToString();
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            location = string.Format("{0}/booth{1}.html", year, index);
            Download();
            candidates = Analyse();
            tpp = candidates.OrderBy(c => c.Place).Take(2).ToArray();
        }

        public Candidate[] Candidates { get { return candidates; } }

        public void Download()
        {
            if (!File.Exists(location))
            {
                client.DownloadFile(string.Format("http://www.ecq.qld.gov.au/elections/state/State{0}/results/booth{1}.html", year, index), location);
            }
        }

        private static void Primaries(Candidate[] candidates, string[] row)
        {
            for (var i = 0; i < candidates.Length; i++)
            {
                candidates[i].Primary = int.Parse(row[i]);
            }
        }

        private static void Transfer(Candidate[] candidates, int place, string[] row)
        {
            var min = candidates.Where(c => c.Place == 0).Min(c => c.Total);
            var victim = candidates.First(c => c.Total == min);
            victim.Place = place;
            for (var i = 0; i < candidates.Length; i++)
            {
                var transferred = int.Parse(row[i]);
                victim.Transfer(candidates[i].Party, transferred);
                candidates[i].Secondary += transferred;
            }
        }

        private Candidate[] Analyse()
        {
            var doc = new HtmlDocument();
            doc.Load(location);
            var table = doc.DocumentNode.SelectSingleNode("//table[normalize-space(tr[1]/td[1]/text())='Summary of Distribution of Preferences']");
            if (table == null)
            {
                if (Verbose)
                {
                    Console.WriteLine("Only two candidates in {0} in {1}", index, year);
                }
                return new Candidate[0];
            }
            var candidates = table.SelectNodes("tr[2]/td/div").ToArray().SkipLast(3).Select(node => new Candidate(index, node.InnerText)).ToArray();
            var rows = table.SelectNodes("tr").Skip(4).ToArray().SkipLast(5).ToArray();
            if (rows.Length == 0)
            {
                if (Verbose)
                {
                    Console.WriteLine("No distribution yet for {0} in {1}", index, year);
                }
                return new Candidate[0];
            }
            Primaries(candidates, rows[0].SelectNodes("td").Skip(1).TakeEveryOther().Select(node => node.InnerText.Trim().Replace(",", "")).ToArray());
            for (var i = 0; i < rows.Length / 2; i++)
            {
                Transfer(candidates, candidates.Length - i,
                    rows[i * 2 + 1].SelectNodes("td").Skip(1).TakeEveryOther().Select(node => node.InnerText.Replace(",", "").Replace("&nbsp;", "0").Replace(" ", "").Trim()).ToArray());
            }
            var remaining = candidates.Where(c => c.Place == 0).OrderByDescending(c => c.Total).ToArray();
            for (var i = 0; i < remaining.Length; i++)
            {
                remaining[i].Place = i + 1;
            }

            return candidates;
        }

        public string TPP()
        {
            var nonexhausted = tpp.Sum(c => c.Total);
            return string.Format("{0,2} {1}", index, string.Join(" vs ", tpp.Select(c => string.Format("{0} ({1:0.00}%)", c.Party, c.Total * 100.0 / nonexhausted))));
        }

        public bool IsLibLab()
        {
            return Liblabs.Contains(tpp[0].Party, StringComparer.OrdinalIgnoreCase) && Liblabs.Contains(tpp[1].Party, StringComparer.OrdinalIgnoreCase);
        }
    }
}
