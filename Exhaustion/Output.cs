﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Exhaustion
{
    public class Output
    {
        private class Election
        {
            private readonly int year;
            public int ALP { get; set; }
            public int LibLabs { get; set; }
            public int Exhaustion { get; set; }
            public int Year { get { return year; } }
            public Election(int year)
            {
                this.year = year;
            }
        }

        private readonly List<Election> elections = new List<Election>();
        private readonly string name;
        private readonly Func<string, bool> predicate;

        public Output(string name, Func<string, bool> predicate)
        {
            this.name = name;
            this.predicate = predicate;
        }

        public void NextElection(int year)
        {
            elections.Add(new Election(year));
        }

        public void Add(string party, int alp, int liblabs, int exhaustion)
        {
            if (!predicate(party))
            {
                return;
            }
            var current = elections.Last();
            current.ALP += alp;
            current.LibLabs += liblabs;
            current.Exhaustion += exhaustion;
        }

        private Tuple<float, float> Draw(Graphics g, Color color, float x1, float x2, Tuple<float, float> oy, float y1, float y2)
        {
            var p = new[]{
                new PointF(x1, oy.Item1),
                new PointF(x2, oy.Item2),
                new PointF(x2, oy.Item2+y2),
                new PointF(x1, oy.Item1+y1),
                new PointF(x1, oy.Item1)
            };
            var brush = new SolidBrush(Color.FromArgb(100, color.R, color.G, color.B));
            var pen = new Pen(color, 2);
            g.FillPolygon(brush, p);
            g.DrawPolygon(pen, p);
            return Tuple.Create(oy.Item1 + y1, oy.Item2 + y2);
        }

        public void Write()
        {
            var data = new List<Tuple<int, double, double, double>>();
            foreach (var election in elections)
            {
                var total = election.LibLabs + election.Exhaustion;
                data.Add(Tuple.Create(election.Year, (double)election.ALP / total, (double)(election.LibLabs - election.ALP) / total, (double)election.Exhaustion / total));
            }
            using (var sw = new StreamWriter(name + ".csv"))
            {
                sw.WriteLine("Year,ALP,LNP,EXH");
                foreach (var election in data)
                {
                    sw.WriteLine("{0},{1},{2},{3}", election.Item1, election.Item2, election.Item3, election.Item4);
                }
            }


            var bmp = new Bitmap(1000, 1000);
            var g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            for (var i = 1; i < data.Count; i++)
            {
                var x1 = (float)((i - 1) * 1000 / (data.Count - 1));
                var x2 = (float)(i * 1000 / (data.Count - 1));
                var prev = Tuple.Create(0F, 0F);
                prev = Draw(g, Color.Red, x1, x2, prev, (float)(data[i - 1].Item2 * 1000), (float)(data[i].Item2 * 1000));
                prev = Draw(g, Color.Blue, x1, x2, prev, (float)(data[i - 1].Item3 * 1000), (float)(data[i].Item3 * 1000));
                prev = Draw(g, Color.Gray, x1, x2, prev, (float)(data[i - 1].Item4 * 1000), (float)(data[i].Item4 * 1000));
            }

            bmp.Save(name + ".png");
            g.Dispose();
            bmp.Dispose();
        }
    }
}