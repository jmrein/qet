﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Exhaustion
{
    public class Candidate
    {
        private readonly static Regex partyValidator = new Regex("^[A-Z]{3,3}$", RegexOptions.Compiled);
        private readonly string surname, party;
        private readonly int booth;
        private readonly Dictionary<string, int> transfer = new Dictionary<string, int>();

        public Candidate(int booth, string text)
        {
            this.booth = booth;
            var parts = text.Split(new[] { '\r', '\n' }).Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => s.Trim().Replace(",", "")).ToArray();
            surname = parts[0];
            party = partyValidator.IsMatch(parts.Last()) ? parts.Last() : "IND";
        }

        public int Primary { get; set; }
        public int Secondary { get; set; }
        public int Total { get { return Primary + Secondary; } }
        public int Place { get; set; }
        public int Booth { get { return booth; } }
        public string Surname { get { return surname; } }
        public string Party { get { return party; } }
        public IDictionary<string, int> Transfers { get { return transfer; } }

        public void Transfer(string to, int amount)
        {
            if (amount > 0)
            {
                //there may be multiple independents running
                transfer.CreateOrAdd(to, amount);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} ({2}) {3} + {4} [{5}]", Place, surname, party, Primary, Secondary, 
                string.Join(", ", transfer.OrderByDescending(c => c.Value).Select(kvp => string.Format("{0} {1}", kvp.Key, kvp.Value))));
        }
    }

}
