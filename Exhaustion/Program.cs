﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Exhaustion
{
    class Program
    {
        private static void CalculateTransfers(Output output, string party, IEnumerable<Candidate> candidates)
        {
            var destinations = new Dictionary<string, int>();
            var total = 0;
            foreach (var candidate in candidates)
            {
                total += candidate.Total;
                foreach (var target in candidate.Transfers)
                {
                    destinations.CreateOrAdd(target.Key, target.Value);
                }
                destinations.CreateOrAdd((candidate.Place < 3) ? "FIN" : "EXH", candidate.Total - candidate.Transfers.Sum(kvp => kvp.Value));
            }
            Console.WriteLine("{0} {1}", party, string.Join(" ", destinations.OrderByDescending(kvp => kvp.Value).Select(d => string.Format("{0} ({1,5:0.00}%)", d.Key, d.Value * 100.0 / total))));
            if (!Booth.Liblabs.Contains(party))
            {
                output.Add(party, destinations["ALP"],
                    destinations.Sum(d => Booth.Liblabs.Contains(d.Key) ? 0 : d.Value),
                    destinations["EXH"]);
            }
        }

        private static void Do(Output output, int year, int n)
        {
            output.NextElection(year);
            var winners = new Dictionary<string, int>();
            var candidates = new List<Candidate>();
            foreach (var booth in Enumerable.Range(1, n).Select(i => new Booth(year, i)))
            {
                if (!booth.Candidates.Any())
                {
                    continue;
                }
                winners.CreateOrAdd(booth.Candidates.First(c => c.Place == 1).Party, 1);
                if (booth.IsLibLab())
                {
                    candidates.AddRange(booth.Candidates);
                }
                else if(Booth.Verbose)
                {
                    Console.WriteLine(booth.TPP());
                }
            }
            if (Booth.Verbose)
            {
                Console.WriteLine();
                Console.WriteLine("===PARLIAMENT ({0})===", year);
                Console.WriteLine(string.Join(Environment.NewLine, winners.OrderByDescending(c => c.Value).Select(w => string.Format("{0} {1}", w.Key, w.Value))));
                Console.WriteLine();
            }
            Console.WriteLine("===TRANSFERS ({0})===", year);
            foreach (var party in candidates.Select(c => c.Party).Distinct())
            {
                CalculateTransfers(output, party, candidates.Where(c => c.Party.Equals(party)));
            }
        }

        private static void Run(Output output)
        {
            Do(output, 2004, 89);
            Do(output, 2006, 89);
            Do(output, 2009, 89);
            Do(output, 2012, 89);
            output.Write();
        }

        static void Main(string[] args)
        {
            Run(new Output("minors", p => !Booth.Liblabs.Contains(p) && !p.Equals("GRN")));
            Run(new Output("greens", p => p.Equals("GRN")));
            Run(new Output("others", p => !Booth.Liblabs.Contains(p)));
        }
    }
}